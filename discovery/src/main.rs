#![no_std]
#![no_main]

use accelerometer::Accelerometer;
use core::fmt::{self, Write};
use cortex_m::peripheral::syst::SystClkSource;
use cortex_m_rt::{entry, exception};
use micromath::F32Ext;
use panic_halt as _;
use stm32f3_discovery::compass::Compass;
use stm32f3_discovery::stm32f3xx_hal::{
    pac::{self, USART1},
    prelude::*,
    serial::Serial,
};
use stm32f3_discovery::wait_for_interrupt;

//define macros for printing directly to the serial port.
macro_rules! uprint {
    ($serial:expr, $($arg:tt)*) => {
        $serial.write_fmt(format_args!($($arg)*)).ok()
    };
}

macro_rules! uprintln {
    ($serial:expr, $fmt:expr) => {
        uprint!($serial, concat!($fmt, "\n"))
    };
    ($serial:expr, $fmt:expr, $($arg:tt)*) => {
        uprint!($serial, concat!($fmt, "\n"), $($arg)*)
    };
}

struct SerialPort {
    usart1: &'static mut pac::usart1::RegisterBlock,
}

impl fmt::Write for SerialPort {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.as_bytes().iter() {
            while self.usart1.isr.read().txe().bit_is_clear() {}
            self.usart1.tdr.write(|w| w.tdr().bits(u16::from(*byte)));
        }
        Ok(())
    }
}

#[entry]
fn main() -> ! {
    let device_periphs = pac::Peripherals::take().unwrap();
    let mut reset_and_clock_control = device_periphs.RCC.constrain();
    let core_periphs = cortex_m::Peripherals::take().unwrap();
    let mut flash = device_periphs.FLASH.constrain();
    let clocks = reset_and_clock_control.cfgr.freeze(&mut flash.acr);

    let (tx, rx) = match () {
        #[cfg(feature = "adapter")]
        () => {
            let mut gpioa = device_periphs.GPIOA.split(&mut reset_and_clock_control.ahb);
            let tx =
                gpioa
                    .pa9
                    .into_af7_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);
            let rx =
                gpioa
                    .pa10
                    .into_af7_push_pull(&mut gpioa.moder, &mut gpioa.otyper, &mut gpioa.afrh);

            (tx, rx)
        }
        #[cfg(not(feature = "adapter"))]
        () => {
            let mut gpioc = device_periphs.GPIOC.split(&mut reset_and_clock_control.ahb);
            let tx =
                gpioc
                    .pc4
                    .into_af7_push_pull(&mut gpioc.moder, &mut gpioc.otyper, &mut gpioc.afrl);
            let rx =
                gpioc
                    .pc5
                    .into_af7_push_pull(&mut gpioc.moder, &mut gpioc.otyper, &mut gpioc.afrl);

            (tx, rx)
        }
    };

    Serial::new(
        device_periphs.USART1,
        (tx, rx),
        115_200.Bd(),
        clocks,
        &mut reset_and_clock_control.apb2,
    );
    let usart1 = unsafe { &mut *(USART1::ptr() as *mut pac::usart1::RegisterBlock) };
    let mut serial = SerialPort { usart1 };

    // setup 1 second systick
    let mut syst = core_periphs.SYST;
    syst.set_clock_source(SystClkSource::Core);
    syst.set_reload(8_000); // period = 1ms
    syst.enable_counter();
    syst.enable_interrupt();

    let mut gpiob = device_periphs.GPIOB.split(&mut reset_and_clock_control.ahb);

    // new lsm303 driver uses continuous mode, so no need wait for interrupts on DRDY
    let mut compass = Compass::new(
        gpiob.pb6,
        gpiob.pb7,
        &mut gpiob.moder,
        &mut gpiob.otyper,
        &mut gpiob.afrl,
        device_periphs.I2C1,
        clocks,
        &mut reset_and_clock_control.apb1,
    )
    .unwrap();

    let mut deltax: f32 = 0.0;
    let mut deltay: f32 = 0.0;
    let mut deltaz: f32 = 0.0;
    let sens: f32 = 0.05;
    loop {
        // let accel = compass.accel_raw().unwrap();
        // uprintln!(serial, "RawAccel:{:?}", accel);
        let normalized_accel = compass.accel_norm().unwrap();
        deltax -= normalized_accel.x;
        deltay -= normalized_accel.y;
        deltaz -= normalized_accel.z;
        if deltax.abs() >= sens || deltay.abs() >= sens || deltaz.abs() >= sens {
            uprintln!(
                serial,
                "{:+.08},{:+.08},{:+.08}",
                normalized_accel.x,
                normalized_accel.y,
                normalized_accel.z
            );
        }
        deltax = normalized_accel.x;
        deltay = normalized_accel.y;
        deltaz = normalized_accel.z;

        // let mag = compass.mag_raw().unwrap();
        // uprintln!(serial, "RawMag:{:?}", mag);

        wait_for_interrupt();
    }
}

#[exception]
fn SysTick() {
    // make sure we don't compile away
    cortex_m::asm::nop();
}
