# Python requirements

In order to run the `getserial.py` program to get information emitted
by the discovery board, the `pyserial` package is needed. To fix this
problem, I decided to create a python virtual environment and include
there all the necessary pypi packages needed for the app execution.

All necessary packages to be installed are included in the
`requirements.txt` file that is also added in the git repo. The actual
folder that includes the virtual environment is under folder
`thesis-env` but as you may have noticed, this folder is added to the
`.gitignore` file, since I don't need to track changes to that.

## Creating the virtual environment

Since the virtual environment folder is not tracked by git, you need
to recreate it in your machine. It would be nice to use the same
folder name, as it is already included in `.gitignore`. To create the
virtual environment, activate it and install the necessary packages,
run the following commands.

``` shell
$ python3 -m venv thesis-env
$ source thesis-env/bin/activate
(thesis-env) $ python -m pip install -r requirements.txt
```

After the above commands are executed, you can now run `getserial.py`
without a problem.

## Deactivating the virtual environment

To deactivate the virtual environment run the command `deactivate`.
