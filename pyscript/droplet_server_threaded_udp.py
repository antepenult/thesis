#!/usr/bin/env python3

from socket import *
from datetime import datetime
import threading
import queue


# with the empty string the server accepts connections on all
# available interfaces
HOST = ''
PORT = 65432
q = queue.Queue()


class ProducerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super().__init__()
        self.target = target
        self.name = name

    def run(self):
        with socket(AF_INET, SOCK_DGRAM) as sock:
            sock.bind((HOST, PORT))
            while True:
                if not q.full():
                    reading, addr = sock.recvfrom(1024)
                    if not reading:
                        break
                    reading = reading.decode('utf-8').strip()
                    timestamp = datetime.utcnow().timestamp()
                    reading = f"{reading},{timestamp:.06f}"
                    print(f"Received: {reading}")
                    q.put(reading)

class ConsumerThread(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super().__init__()
        self.target = target
        self.name = name

    def run(self):
        with open("results.csv", "w") as f:
            f.write("x-accel,y-accel,z-accel,source_time,receive_time,use_time\n")
            while True:
                try:
                    reading = q.get(timeout=30)
                    timestamp = datetime.utcnow().timestamp()
                    reading = f"{reading},{timestamp:.06f}"
                    print(f" Processed: {reading}")
                    f.write(reading+'\n')
                except queue.Empty:
                    print("Connection closed or timed out (30 secs)")
                    return

if __name__ == '__main__':

    p = ProducerThread(name='producer')
    c = ConsumerThread(name='consumer')

    p.start()
    c.start()
