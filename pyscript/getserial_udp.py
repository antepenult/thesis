#!/usr/bin/env python3
from datetime import datetime
import sys
import serial
import serial.tools.list_ports
import socket

HOST = '143.110.211.89' # The IP address of the droplet in digitalocean
PORT = 65432 # The communication port

def get_device():
    """
Returns the device name to be used for communication with the board.

At the moment this is setup for linux and macOS. I am using a simple
regex to find the devices. Usually the device appears as
cu.usbmodemXXXXX in macOS and as ttyACMxx in linux, hence the regex
used in grep.
"""
    device = serial.tools.list_ports.grep(r"cu\.usb|ttyACM")
    try:
        dev = next(device)
        return dev.device
    except StopIteration:
        print("Board not found")
        sys.exit()


def read_serial(serial_port):
    """Get a serial port, read a line, add a timestamp and then return a
formatted string. This was originally in the main loop, but had to
bring it out for ease of use.

    """
    reading = serial_port.readline().decode('utf-8').strip()
    timestamp = datetime.now().timestamp()
    return f"{reading},{timestamp:.06f}".encode('ascii')


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
    with serial.Serial(get_device(), 115200) as ser:
        while True:
            line = read_serial(ser)
            print(line)
            sock.sendto(line, (HOST, PORT))
