#!/user/bin/env python3

from socket import *
from datetime import datetime

# with the empty string the server accepts connections on all
# available interfaces
HOST = ''
PORT = 65432

def droplet_server(address):
    with socket(AF_INET, SOCK_STREAM) as sock:
        sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        sock.bind(address)
        sock.listen(5)
        client, addr = sock.accept()
        print('Connected by', addr)
        sock_handler(client)

def sock_handler(client):
    with open("results.csv", "w") as f:
        f.write("x-accel,y-accel,z-accel,source_time,destination_time\n")
        while True:
            req = client.recv(53)
            if not req:
                break
            req = req.decode('utf-8').strip()
            timestamp = datetime.utcnow().timestamp()
            req = f"{req},{timestamp:.06f}"
            print(req)
            f.write(req+'\n')
    print("Closed")

droplet_server((HOST, PORT))
